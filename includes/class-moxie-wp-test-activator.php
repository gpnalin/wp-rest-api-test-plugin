<?php

/**
 * Fired during plugin activation
 *
 * @link       http://nalin.xyz
 * @since      1.0.0
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 * @author     Nalin Perera <gpnalin@gmail.com>
 */
class Moxie_WP_Test_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		
		
	}

}
